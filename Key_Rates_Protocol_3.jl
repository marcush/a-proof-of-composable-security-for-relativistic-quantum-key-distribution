using LinearAlgebra # For Linear Algebra functionality
using MathOptInterface # Abstraction layer for Mathematical Optimization, replacement for the one first used in JuMP
using JuMP # Collection of Optimizers for Mathematical Optimization
using Hypatia # Interior Point Solver for Convex Problems
using Hypatia.Cones # Used for definition of Cones for Solvers
using Mosek # on-shelf optimization software
using MosekTools # Tools for Mosek
using QuantumInformation # Implementation of the most usefull QIT quantities
using LaTeXStrings
using Optim
using Plots

# ------- PROTOCOL 3 ------- #

# Defining our security parameters

eps_kv = 5e-11
eps_pa = 1e-10
eps_a = 4e-10
eps_s = 2e-10
eps_comp_kv = 5e-3
eps_comp_ev = 5e-3


# Defining our measurement operators

bellplus = [0,1,1,0] / √(2)
bellminus = [0,1,-1,0] / √(2)
dc = [0,0,0,1]
vac = [1,0,0,0]
M0 = kron(bellplus,bellplus') + .5 * kron(dc,dc') # conc 1
M1 = kron(bellminus,bellminus') + .5 * kron(dc,dc') # conc 2
M_bot = kron(vac,vac') # inconc

Gamma_fail = kron([1 0; 0 0], M1) + kron([0 0; 0 1], M0)
Gamma_inconc = kron([1 0; 0 1], M_bot)
Gamma_corr = I - Gamma_fail - Gamma_inconc

Gs = [Gamma_corr,Gamma_fail,Gamma_inconc]


# We set our canonical lexographic basis: {|0>, |1>} and {|00>, |01>, |10>, |11>}

# Bob's measurements
# v = Outcome of Bob's measurement: {1 if Alice prepared state 1, 2 if Alice prepared state 2, 3 if inconclusive
# c = Outcome of the statistic: {1 if Conclusive correct; 2 if Conclusive incorrect, 3 if Inconclusive
# the parameter gamma from the paper is called mu here
# due to numerical stability issues here we can only tolerate mu >= 1e-4,
# but this is not a fundamental limitation


# --- General Security through Generalized Entropy Accumulation --- #
# Part 1: Computations

# --- General Security through Generalized Entropy Accumulation --- #
# Part 1: Computations

# performs the optimization for finite-size key rates
function finite_size_kr_P3(n,d,eta,q,opt_start_vector=[0,0,0,0,0],num_max=50)

    # Compute a new start point if none is available
    if opt_start_vector == [0,0,0,0,0]
        mu0 = 0.01;
        g_lam0 = lambda_vec(d, eta, q, mu0) # compute numerical derivative for testing probability mu
        g_c = c_lambda_P3(d, eta, q, mu0, g_lam0) # compute lower bound c_lambda_P3
        ca_max, ca_min, ca_min_sigma, ca_var = ca_properties(g_lam0, g_c, mu0)
        alpham1_start = alpham1(n, ca_var)/100  #the factor 1000 is based off experience, not thought
        opt_start_vector =  [alpham1_start, mu0, g_lam0...]
    end

    # calculate the new key rate
    kr_optim(opts) = kr_optimizer_P3(opts,n,d,eta,q)
    opts = Optim.optimize(kr_optim,opt_start_vector,NelderMead(),Optim.Options(f_abstol=1e-2,f_reltol=1e-2,x_reltol=1e-2,f_calls_limit=num_max)) # returns the negative key rate
    opt_alpha_mu_lambda = opts.minimizer
    opt_rate =  -opts.minimum

    println("Protocol 3: Found for start-vector: $(opt_start_vector) ...")
    println("an optimal key rate: $(opt_rate), for alpha-1: $(opt_alpha_mu_lambda[1]), mu: $(opt_alpha_mu_lambda[2]), lambda: $(opt_alpha_mu_lambda[3:5])")
    
    return [opt_rate,opt_alpha_mu_lambda]
end

# creates the overlap figure
function finite_size_kr_P3_overlap(steps,nn=[1e12,1e9],num=50)
    #compute key rates by numerically optimizing over parameters
    ds_high = LinRange(0.9,1,steps+1)[1:steps] # kill d=1
    ds_low = reverse(LinRange(0.65,.9,steps+2)[2:steps+1]) # kill d=0, d=0.7

    key_rates_high = zeros(length(nn), steps) # one row per n
    key_rates_low = zeros(length(nn), steps)
    optimizers_all = zeros(length(nn), 2*steps, 5)

    for j in 1:length(nn)
        n = nn[j]
        println("----------> Now starting n = $(n) of $(nn) <----------")
        println("-------> Now starting downwards <--------")
        for k in 1:steps
            num_max = num
            d = ds_high[k]
            println("d: $(d)")
            if k == 1 # check whether we're in the first round for a given n, if so get initial conditions manually, otherwise we just take the solution from the previous round
                num_max = 12*num
                opt_start_vector = [0,0,0,0,0]#[3.397596711091969e-6,0.00018868101912858858,-0.005312093331408777,0.0153382114562647,-0.0020134309468305767]# # Optimize manually
            else
                opt_start_vector = optimizers_all[j,k-1,:] # Take the optimal from close overlap
            end
            
            if (k > 1 && key_rates_high[j,k-1] < 1e-7) || (j > 1 && key_rates_high[j-1,k] < 1e-7) # theres not really a point in continuing
                key_rates_high[j,k] = 0
            else
                # Optimize over alpha, mu, and lambda
                key_rates_high[j,k], optimizers_all[j,k,:] = finite_size_kr_P3(n,d,1.,0.,opt_start_vector,num_max)
            end
        end

        println("----------> Halfway through n = $(n) of $(nn) <----------")
        println("-------> Now starting downwards <--------")
        for k in 1:(steps)
            num_max = num
            d = ds_low[k]
            println("d: $(d)")
            if k == 1 # check whether we're in the first round for a given n, if so get initial conditions from upwards, otherwise we just take the solution from the previous round
                opt_start_vector = optimizers_all[j,1,:]
            else
                opt_start_vector = optimizers_all[j,k+steps-1,:]
            end
            
            if (k > 1 && key_rates_low[j,k-1] < 1e-7) || (j > 1 && key_rates_low[j-1,k] < 1e-7) # theres not really a point in continuing
                key_rates_low[j,k] = 0
            else
                # Optimize over alpha, mu, and lambda
                key_rates_low[j,k], optimizers_all[j,k+steps,:] = finite_size_kr_P3(n,d,1.,0.,opt_start_vector,num_max)
            end
        end
    end

    ds = vcat(reverse(ds_low),ds_high)
    key_rates = hcat(reverse(key_rates_low,dims=2),key_rates_high)
    clamp!(key_rates,0,Inf)
    return [ds,key_rates,optimizers_all]
end

function optimal_overlap()
    func(d) = -asymp_key_rates_P3(d,1.,0.)
    opts = Optim.optimize(func,.9,.95)
    d = opts.minimizer
    kr =  -opts.minimum

    println("Maximal key rate $(kr) for d=$(d)")
    return d
end

function finite_size_kr_P3_loss(steps,nn=[1e12,1e7],d=0.6911156094384834,num=50)
    #compute key rates by numerically optimizing over parameters
    etas = reverse(LinRange(0,1,steps+1)[2:steps+1]) # kill eta=0

    key_rates = zeros(length(nn), steps) # one row per n
    optimizers_all = zeros(length(nn), steps, 5)
    for j in 1:length(nn)
        n = nn[j]
        println("----------> Now starting n = $(n) of $(nn) <----------")
        for k in 1:steps
            eta = etas[k]
            num_max = num
            println("η: $(eta)")
            if k == 1 # check whether we're in the first round for a given n, if so get initial conditions manually, otherwise we just take the solution from the previous round
                num_max = 10*num
                if j == 1
                    opt_start_vector = [0,0,0,0,0] #[3.397596711091969e-6,0.00018868101912858858,-0.005312093331408777,0.0153382114562647,-0.0020134309468305767]# # Optimize manually
                else
                    opt_start_vector = optimizers_all[j-1,1,:] # Take the optimal from same overlap for more rounds
                end
            else
                opt_start_vector = optimizers_all[j,k-1,:] # Take the optimal from close overlap
            end
            
            if (k > 1 && key_rates[j,k-1] < 1e-7) || (j > 1 && key_rates[j-1,k] < 1e-7) # theres not really a point in continuing
                key_rates[j,k] = 0
            else
                # Optimize over alpha, mu, and lambda
                key_rates[j,k], optimizers_all[j,k,:] = finite_size_kr_P3(n,d,eta,0.,opt_start_vector,num_max)
            end
        end
    end

    clamp!(key_rates,0,Inf)
    return [reverse(etas),reverse(key_rates,dims=2),d,optimizers_all]
end


# --- Pretty plots --- #

# overlap plot
function plot_kr_overlap_P3(ds,key_rates, labels = [L"n=10^{12}" L"n=10^7"])

    len = length(ds)
    n = 2*len
    ds_2 = range(0.6,1,n+2)[begin+1:end-1]

    Ks = zeros(n)
    est_Ks = zeros(n)

    for i in 1:n
        Ks[i] = asymp_key_rates_P3(ds_2[i], 1.,0.)
        est_Ks[i] = conjecture(ds_2[i], 1.,0.)
    end

    p = Plots.plot(ds_2, [Ks, est_Ks],
        label=["Asymptotic rate "*L"n=\infty" "Conjectured rate"], 
        dpi=900,legend=:left)
    Plots.plot!(ds, key_rates', label=labels)
    Plots.xlabel!(p,"Overlap " * L"\ d=\langle \Psi_0\ |\Psi_1 \rangle_{SR}")
    Plots.ylabel!(p,"Key Rate " * L"\ K(n)")
    Plots.title!(p,"Key rates for Protocol 3, noiseless")
    Plots.savefig(p, "Protocol_3_noiseless.png")

    return p
end

# --- Pretty plots --- #
function plot_kr_loss_P3(etas,key_rates,labels = [L"n=10^{12}" L"n=10^7"],d=0.6911156094384834)

    len = length(etas)
    n = 2*len
    etas_2 = LinRange(0,1,n+1)[2:n+1]

    Ks = zeros(n)
    est_Ks = zeros(n)

    for i in 1:n
        Ks[i] = asymp_key_rates_P3(d, etas_2[i],0.)
    end

    p = plot(etas_2, Ks,
        label="Asymptotic rate "*L"n=\infty", 
        dpi=900,legend=:topleft)
    plot!(etas, key_rates', label=labels)
    xlabel!(p,"Transmittance " * L"\eta")
    ylabel!(p,"Key Rate " * L"\ K(n)")
    title!(p,"Key rates for Protocol 3, lossy channel, "*L"d=0.90")
    savefig(p, "Protocol_3_losses.png")

    return p
end

# --- General seurity with Generalized Entropy Accumulation --- #
# --- Part 2: Functionality --- #

"""
Computation of a lower bound on the constant offset c_lambda
through Eve's attack for a given statistics, testing probability,
and linear term lambda
"""
function c_lambda_P3(d, eta, q, mu, lambda)
    # Initialize the optimizer and model
    tol = 1e-7
    optimizer = Hypatia.Optimizer(;
        verbose=false,
        tol_feas=tol,
        tol_infeas=tol,
        default_tol_relax=tol,
        default_tol_power=tol
    )
    model = Model(() -> optimizer)

    dim_P = dim_S = dim_R = 2 # P - Alice's raw key bit, S - sent signal state ∈ {|±α>}, R - sent reference |α>
    dim_A = dim_S * dim_R # Alice's signal state
    dim_B = dim_S * dim_R # Eve's channel: SR = A → SR = B, so from dim 4 to dim 4
    dim_AB = dim_A * dim_B # Choi state 16x16 matrix

    # The choi state
    vec_dim = Cones.svec_length(dim_AB) # Free variables for real Symmetric matrix of size 16x16
    @variable(model, rho_cj_vec[1:vec_dim]) # Choi state to be optimized over in vector notation of free variables
    rho_cj = zeros(JuMP.AffExpr, dim_AB, dim_AB) # Choi state as matrix
    Cones.svec_to_smat!(rho_cj, 1.0 * rho_cj_vec, 1) # Save vector rho_cj_vec as upper triangle mat
    rho_cj = Symmetric(rho_cj) # Create Choi state

    # The constraints on the choi matrix
    @constraint(model, t1, tr(rho_cj) == 1) # global trace condition
    @constraint(model, cp, rho_cj in PSDCone()) # rho_cj should be hermetian for CP!
    @constraint(model, t2, ptrace(rho_cj, 2, [dim_A, dim_B]) .== mixed(dim_A)) # tr_B(rho_cj) .== mixed(dimA) for TP, local trace condition for TP

    # The non-signaling constraints -> Holds up
    dims = [dim_S, dim_R, dim_S, dim_R]
    lhs = ptrace(rho_cj, 3, dims) # Trace of complement of sent
    rhs = kron(mixed(dim_S), ptrace(rho_cj, [1, 3], dims)) # Trace of complement of sent, and input signal tensor mixed(S)
    @constraint(model, ns, lhs .== rhs)

    # Compute the total channel applied to the state, i.e. the choi state E(I_P⊗E')
    E = choi_tensor(choi_id(dim_P), dim_P, dim_P, convert(Matrix{AffExpr}, rho_cj), dim_A, dim_B)

    # The inputs to the map
    phi_PSR = phi_in_P3(d)

    # Apply the channel
    rho_PSR = apply_choi_to_state(E,phi_PSR,dim_P*dim_A,dim_P*dim_B)

    # Apply the virtual channels through measurements
    X_nu = nu((1-mu)*rho_PSR)
    pinched_nu = pinch_P(X_nu)

    # Convert to vectors
    vec_dim2 = Cones.svec_length(size(X_nu)[begin])
    X_nu_vec = zeros(JuMP.AffExpr, vec_dim2)
    Cones.smat_to_svec!(X_nu_vec, X_nu, √(2))

    pinched_nu_vec = zeros(JuMP.AffExpr, vec_dim2)
    Cones.smat_to_svec!(pinched_nu_vec, pinched_nu, √(2))

    # Add the measurement constraints
    Probs_hon = prepare_statistics(eta,q,d)
    for v in 1:3
        @constraint(model, tr(Gs[v] * rho_PSR) == Probs_hon[v])
    end

    # Slack variable for relative entropy cone
    @variable(model, y)

    # Subtract linear term
    @objective(model, Min, y / log(2) - dot(lambda,Probs_hon)) # Set objective

    # Add relative entropy constraint
    entropy_cone = EpiTrRelEntropyTriCone{Float64}(1 + 2 * vec_dim2)
    @constraint(model, [y; pinched_nu_vec; X_nu_vec] in entropy_cone)

    #print(model)

    # Run the optimization
    optimize!(model)

    if termination_status(model) != MathOptInterface.OPTIMAL
        @show termination_status(model)
    end

    #@show dual_objective_value(model) - objective_value(model)

    ent = objective_value(model)

    return ent
end


"""
Computation of the minimal entropy through Eve's 
attack for a given statistics and testing probability
"""
function opt_entropy_P3(d, eta, q, mu,stats=[0,0,0])
    # Initialize the optimizer and model
    tol = 1e-7
    optimizer = Hypatia.Optimizer(;
        verbose=false,
        tol_feas=tol,
        tol_infeas=tol,
        default_tol_relax=tol,
        default_tol_power=tol
    )
    model = Model(() -> optimizer)

    dim_P = dim_S = dim_R = 2 # P - Alice's raw key bit, S - sent signal state ∈ {|±α>}, R - sent reference |α>
    dim_A = dim_S * dim_R # Alice's signal state
    dim_B = dim_S * dim_R # Eve's channel: SR = A → SR = B, so from dim 2 to dim 4
    dim_AB = dim_A * dim_B # Choi state 8x8 matrix

    # The choi state
    vec_dim = Cones.svec_length(dim_AB) # Free variables for real Symmetric matrix of size 16x16
    @variable(model, rho_cj_vec[1:vec_dim]) # Choi state to be optimized over in vector notation of free variables
    rho_cj = zeros(JuMP.AffExpr, dim_AB, dim_AB) # Choi state as matrix
    Cones.svec_to_smat!(rho_cj, 1.0 * rho_cj_vec, 1) # Save vector rho_cj_vec as upper triangle mat
    rho_cj = Symmetric(rho_cj) # Create Choi state

    # The constraints on the choi matrix
    @constraint(model, t1, tr(rho_cj) == 1) # global trace condition
    @constraint(model, cp, rho_cj in PSDCone()) # rho_cj should be hermetian for CP!
    @constraint(model, t2, ptrace(rho_cj, 2, [dim_A, dim_B]) .== mixed(dim_A)) # tr_B(rho_cj) .== mixed(dimA) for TP, local trace condition for TP

    # The non-signaling constraints -> Holds up
    dims = [dim_S, dim_R, dim_S, dim_R]
    lhs = ptrace(rho_cj, 3, dims) # Trace of complement of sent
    rhs = kron(mixed(dim_S), ptrace(rho_cj, [1, 3], dims)) # Trace of complement of sent, and input signal tensor mixed(S)
    @constraint(model, ns, lhs .== rhs)

    # Compute the total channel applied to the state, i.e. the choi state E(I_P⊗E')
    E = choi_tensor(choi_id(dim_P), dim_P, dim_P, convert(Matrix{AffExpr}, rho_cj), dim_A, dim_B)

    # The inputs to the map
    phi_PSR = phi_in_P3(d)

    # Apply the channel
    rho_PSR = apply_choi_to_state(E,phi_PSR,dim_P*dim_A,dim_P*dim_B)

    # Apply the virtual channels through measurements
    X_nu = nu((1-mu)*rho_PSR)
    pinched_nu = pinch_P(X_nu)

    # Convert to vectors
    vec_dim2 = Cones.svec_length(size(X_nu)[begin])
    X_nu_vec = zeros(JuMP.AffExpr, vec_dim2)
    Cones.smat_to_svec!(X_nu_vec, X_nu, √(2))

    pinched_nu_vec = zeros(JuMP.AffExpr, vec_dim2)
    Cones.smat_to_svec!(pinched_nu_vec, pinched_nu, √(2))

    # Slack variable for relative entropy cone
    @variable(model, y)
    @objective(model, Min, y / log(2)) # Set objective

    # Add relative entropy constraint
    entropy_cone = EpiTrRelEntropyTriCone{Float64}(1 + 2 * vec_dim2)
    @constraint(model, [y; pinched_nu_vec; X_nu_vec] in entropy_cone)

    # Add the measurement constraints
    if stats == [0,0,0]
        Probs_hon = prepare_statistics(eta,q,d)
    else
        Probs_hon = stats
    end

    for v in 1:3
        @constraint(model, tr(Gs[v] * rho_PSR) == Probs_hon[v])
    end

    #print(model)

    # Run the optimization
    optimize!(model)

    if termination_status(model) != MathOptInterface.OPTIMAL
        @show termination_status(model)
    end

    #@show dual_objective_value(model) - objective_value(model)

    ent = objective_value(model)

    println("Entropy: $(ent) for d=$(d), eta=$(eta), q=$(q)")

    return ent
end

# computes the key rate in the asymptotic limit for mu = 0
function asymp_key_rates_P3(d, eta, q)
    return opt_entropy_P3(d, eta, q, 0.) - ec_conditional_entropy(d,eta,q)
end


"""
phi_PQ(alpha, eta)
The honest state (in the entanglement based version of the protocol):

returns |Ψ><Ψ|, where

|Ψ> = ([1,0]_A ⊗ in_state(+1,p) + [0,1]_A ⊗ in_state(-1,p)) / √(2)
"""
function phi_in_P3(d)
    z = [1, 0] # P: z ≡ {a = 0}, o ≡ {a = 1}
    o = [0, 1]


    phi = (kron(z, sqrt(d)*vac + sqrt(1-d)*bellplus) + kron(o, sqrt(d)*vac + sqrt(1-d)*bellminus)) / sqrt(2) # Alice's prepared state PSR
    return phi * phi' # 8x8
end


# function wrapper for optimization of alpha-1, mu and lambda_vec
function kr_optimizer_P3(alpha_and_mu_and_g_lam,n=10^3,d=.7, eta=.7, q=0.) #outputs negative rate!!
    alpm1 = alpha_and_mu_and_g_lam[1]
    mu_fix = alpha_and_mu_and_g_lam[2]
    if mu_fix > 0 && mu_fix < 1 && alpm1 > 0 && alpm1 < 0.5
        g_lam = alpha_and_mu_and_g_lam[3:5]

        # Compute c_lambda_P3
        g_c = c_lambda_P3(d, eta, q, mu_fix, g_lam)

        # Compute properties and minimum entropy
        ca_max, ca_min, ca_min_sigma, ca_var = ca_properties(g_lam, g_c, mu_fix)
        kca = k_ca(n, d, eta, q, ca_max, ca_min, ca_var, g_lam, g_c) # TODO!!!
        sec_order = rate_second_order(n, d, eta, q, alpm1, ca_max, ca_min_sigma, ca_var)
        KR = (kca - sec_order)
        #println("Key rate for optimization: $(KR)")
        return -KR
    else
        return 100
    end
end


# compute the second order terms
function rate_second_order(n, d, eta, q, alpham1, ca_max, ca_min_sigma, ca_var)
# second order terms need to be subtracted from k_ca
    t1 = alpham1/(1 - alpham1) * log(2)/2 * capital_V(ca_var)^2;
    t2 = (g_eps(eps_s) + (1 + alpham1) * log(1/eps_a))/(alpham1*n);
    t3 = (alpham1/(1 - alpham1))^2 * capital_K(alpham1, ca_max, ca_min_sigma);
    t4 = lambda_ec_rate(n, d, eta, q);
    t5 = ceil(2*log2(1/eps_pa))/n + ceil(log2(1/eps_kv))/n;
    return t1 + t2 + t3 + t4 + t5;
end

function capital_V(ca_var)
    # for us, d_A = |S| = 3
    return log2(19) + sqrt(2 + ca_var);
end

function capital_K(alpham1, ca_max, ca_min_sigma)
    f1 = (1 - alpham1)^3/(6*(1 - 2 * alpham1)^3 * log(2));
    f2 = alpham1/(1 - alpham1) * (2*log2(3) + ca_max - ca_min_sigma);
    f3 = 2^(2*log2(3) + ca_max - ca_min_sigma) + exp(2);
    return f1 * 2^f2 * log(f3)^3;
end

function g_eps(eps)
    return log2(2/eps^2);
    #return -log2(1 - sqrt(1 - eps^2));
end

# first guess for optimal alpha (its not that good...)
function alpham1(n, ca_var)
    return 2/sqrt(n)*sqrt(log2(2/(eps_s*eps_a))/ca_var)
end

function lambda_ec_rate(n,d,eta,q) # rate lost to error correction
    return ec_loss(d,eta,q) + 2 / sqrt(n) * sqrt(1 - 2*log2(eps_comp_kv/2)) * log2(7) + 2 * log2(2/eps_comp_kv) / n;
end

function k_ca(n, d, eta, q, ca_max, ca_min, ca_var, g_lambda, g_c)
    base_rate = dot(prepare_statistics(eta,q,d),g_lambda) + g_c # g(q') = f(q) as per Lem V.5
    deltasq = (2*(ca_max - ca_min)*base_rate + 6*ca_var) / (3*n) * log2(1/(eps_comp_kv - eps_kv))
    return base_rate - sqrt(deltasq)
end

function ca_properties(g_lambda, g_c, mu)
    # first calculate the properties of g
    g_max = maximum(g_lambda) + g_c
    g_min = minimum(g_lambda) + g_c
    # disp('gmax')
    # disp(g_max)
    # disp('gmin')
    # disp(g_min)

    # now comptue the properties of CA as in Prop V.5 of the Omar Fred paper
    # (called f there). It's easy to check that the ineqs from Prop V.5 go in
    # the right direction, so we treat them as equality.
    ca_max = g_max;
    ca_min = (1 - 1/mu)*g_max + 1/mu * g_min;
    ca_min_sigma = g_min;
    ca_var = 1/mu * (g_max - g_min)^2;
    # disp("[ca_max, ca_min, ca_min_sigma, ca_var]")
    # disp([ca_max, ca_min, ca_min_sigma, ca_var])
    return [ca_max, ca_min, ca_min_sigma, ca_var]
end

function ei(k,n)
    e = zeros(n)
    e[k] = 1
    return e
end

# Computes quite a bad numerical derivative, this thing breaks easier then my childhood dreams
function lambda_vec(d, eta, q, mu,del=1e-3)
    nu_C = prepare_statistics(eta,q,d)
    entropy_base = opt_entropy_P3(d,eta,q,mu)
    kmax = 5*3
    pert_nu = zeros(kmax,3)
    entropy_pert = zeros(kmax)
    for k=1:kmax
        nup = rand(3)'
        nup ./= sum(nup)
        ns = (1 - del)*nu_C + del*nup
        pert_nu[k,:] = ns - nup
        entropy_pert[k] = opt_entropy_P3(d,eta,q,mu,ns) - entropy_base
    end
    lambda = pert_nu \ entropy_pert
    println("Numerical derivative lambda: $(lambda)")
    return lambda
end

# calculates the expected statistics for a noisy honest implementation
# at noise level p
function nu_star(psi)
    stats = zeros(3)
    for v in 1:3
        stats[v] = tr(Gs[v] * psi)
    end
    return stats
end


"""
The combined measurements between Alice and Bob, returns matrices Gamma_corr for correct key bit u == v,
Gamma_fail for failure event u != v, and Gamma_inconc for v == 3 outcome
"""

# computes bell state |01> + (ind_sign)|10> / sqrt(2)
function bell_state(ind_sign)
    @assert ind_sign in [+1.,-1.]
    return [0,1,ind_sign,0] / √(2)
end


## Get the maximally mixed state of dimension dim
mixed(dim::Int) = Matrix(I, dim, dim) / dim

function ec_conditional_entropy(d,eta,q)
    p_inc = d^eta
    return (1-p_inc)*h_bin(q)
end


function ec_loss(d,eta,q)
    Probs_hon = prepare_statistics(eta,q,d)

    p_conc = 1 - Probs_hon[3]
    QBER = Probs_hon[2] / p_conc
    return p_conc*h_bin(QBER)
end

function prepare_statistics(eta,q,d)
    p_inc = d^eta
    err = q*(1-p_inc)
    succ = 1-err-p_inc
    stats = [succ err p_inc]
    return stats
end

function apply_choi_to_state(choi::Matrix{T}, state::Matrix{U}, dim_A::Int, dim_B::Int) where {T,U}
    return dim_A*ptrace((kron(transpose(state),Matrix(I,dim_B,dim_B))*choi), 1, [dim_A, dim_B])
end

function randH(n::Int)
    H = 2*(randn(n,n) + im*randn(n,n)) .- (1+im)
    return H + H'
end

function randrho(n::Int)
    p = 10*randH(n)
    return p*p'/tr(p*p')
end

"""
Returns the perfect shuffle matrix S_dc,db, see wikipedia
"""
function perfect_shuffle(d_c::Int, d_b::Int)
    r = d_c*d_b
    Ir = Matrix(I, r, r)
    return vcat([Ir[j:d_b:r,:] for j in 1:d_b]...)
end

function projector(v)
    return kron(v,v')
end

function single_one(n,i,j)
    M = zeros(n,n)
    M[i,j] = 1
    return M
end

"""
flip_C_B(E_tensor_F::Matrix{T}, d_a::Int, d_b::Int, d_c::Int, d_d::Int)

Returns C(E⊗F), where E: H_A ↦ H_B, F: H_C ↦ H_D
and E_tensor_F = C(E)⊗C(F)
Equivalently: Flips matrixes E_tensor_F = A⊗C⊗B⊗D ↦ A⊗B⊗C⊗D
"""
function flip_C_B(E_tensor_F::Matrix{T}, d_a::Int, d_b::Int, d_c::Int, d_d::Int) where {T}
    m, n = size(E_tensor_F)
    @assert m == n "Not rectangular matrix passed"
    @assert d_a*d_b*d_c*d_d == m "Not right dimensions"
    S_dc_db = perfect_shuffle(d_c,d_b)
    laction = kron(Matrix(I,d_a,d_a),kron(S_dc_db,Matrix(I,d_d,d_d)))'
    raction = laction'
    return laction * E_tensor_F * raction
end

"""
choi_tensor(E::Matrix{T},d_a::Int, d_b::Int,F::Matrix{T},d_c::Int, d_d::Int)

Returns C(E⊗F), where C(E) complex d_a*d_b x d_a*d_b matrix, E: H_A ↦ H_B, and
C(F) complex d_c*d_d x d_c*d_d matrix, F: H_C ↦ H_D
"""
function choi_tensor(CE::Matrix{T},d_a::Int, d_b::Int,CF::Matrix{U},d_c::Int, d_d::Int) where {T,U}
    mE, nE = size(CE)
    mF, nF = size(CF)
    @assert d_a*d_b == mE && mE == nE "Choi mat E doesn't work out"
    @assert d_c*d_d == mF && mF == nF "Choi mat F doesn't work out"
    return flip_C_B(kron(CE,CF),d_a,d_b,d_c,d_d)
end

"""
    inv_choi_isom(ρ, dim_A, dim_B)

Compute the basis dependant matrix E, such that Vec(Eρ) = E*Vec(ρ), encompassing a specified Choi state ρ = C(E')
"""
function inv_choi_isom(rho::Matrix{T}, dim_A::Int, dim_B::Int) where {T}
    m, n = size(rho)
    @assert m == n
    @assert dim_A * dim_B == m

    res = zeros(T, dim_B^2, dim_A^2)
    for i in 1:dim_A, j in 1:dim_A
        u = i + (j - 1) * dim_A

        # Build the matrix S_A
        S_A = zeros(dim_A, dim_A)
        S_A[i, j] = 1

        op = kron(S_A', Matrix(I, dim_B, dim_B)) * rho
        rho_out = dim_A * ptrace(op, 1, [dim_A, dim_B])
        res[:, u] = vec(rho_out)
    end

    return res
end

"""
Returns the Choi matrix of the Identity on dim-dimensional space
"""
function choi_id(dim::Int)
    Id = zeros(dim^2,dim^2)
    for i in 1:dim, j in 1:dim
        Id[(i-1)*dim + i,(j-1)*dim + j] = 1
    end
    return Id / dim
end

"""
Build a square matrix out of a square length vector
"""
function mat(v::Vector{T}) where {T}
    n = length(v)
    m = isqrt(n)
    @assert m^2 == n

    return reshape(v, (m, m))
end


"""
nu(phi, alpha, eta)

Nu as in GEAT paper
"""
function nu(phi)
    op_PSR = sqrt(I - Gamma_inconc)
    return op_PSR*phi*op_PSR
end


## Pinching map applied to some state nu
function pinch_P(nu)
    res = zeros(size(nu))
    id_SR = Matrix(I, 2*2, 2*2)

    for u in 1:2
        vec = Matrix(I, 2, 2)[:, u] # [1, 0] or [0, 1]
        proj = vec * vec' 
        op = kron(proj, id_SR) # Π ⊗ id
        res += op*nu*op
    end

    return res
end


# returns conditional entropy over subsystem ind 
function conditional_entropy(rho_AB, ind, dims)
    return vonneumann_entropy(rho_AB) - vonneumann_entropy(ptrace(rho_AB,ind,dims))
end

## binary entropy
function h_bin(p)
    if p == 0 || p == 1
        return 0
    elseif p < 0 || p > 1
        return 100
    end

    return -p * log2(p) - (1 - p) * log2(1 - p)
end


## Probability of not receiving an inconclusive outcome
function success_prob(d,eta)
    return d^eta
end

"""
ptrace(A::AbstractMatrix{T}, axis::Int, dims::Vector{Int}) where {T}

Partical trace over Subsys #axis of Dimensions [dim[Subsys#0], dim[Subsys#1], ...]
"""
function ptrace(A::AbstractMatrix{T}, axis::Int, dims::Vector{Int}) where {T}
    dim_l = prod(dims[begin:axis-1])
    dim_r = prod(dims[axis+1:end])
    new_dims = (dim_r, dims[axis], dim_l, dim_r, dims[axis], dim_l)
    B = reshape(A, new_dims)

    res::Matrix{eltype(A)} = zeros(eltype(A), dim_l * dim_r, dim_l * dim_r)
    for i in eachindex(@view B[begin, begin:end, begin, begin, begin, begin, begin])
        tmp = @view B[begin:end, i, begin:end, begin:end, i, begin:end]
        res += reshape(tmp, size(res))
    end

    return res
end


function ptrace(A::AbstractMatrix{T}, axes::Vector{Int}, dims::Vector{Int}) where {T}
    dims = copy(dims)
    res::Matrix{eltype(A)} = A

    for axis in axes
        res = ptrace(res, axis, dims)
        dims[axis] = 1
    end

    return res
end

function conjecture(d,eta,q)
    return (1-d^eta)*(1-h_bin((1-d)/2)-h_bin(q))
end

function nsc_squash(n=5)
    ds = range(0.65,1,n+1)[begin:end-1]
    Ks = zeros(n)
    est_Ks = zeros(n)

    for i in 1:n
        Ks[i] = asymp_key_rates_P3(ds[i], 1.,0.)
        est_Ks[i] = conjecture(ds[i], 1.,0.)
    end

    p = plot(ds, [Ks, est_Ks], label=["Attack channel analysis" "Entropic analysis"], dpi=900, legend=:topleft)
    xlabel!(p,"Overlap" * L"\ d=\langle \Psi_0\ |\Psi_1 \rangle_{SR}")
    ylabel!(p,"Asympt. Key Rate" * L"\ K_\infty=H(A|EC)_{|\Omega}")
    title!(p,"Relativistic constraints for noiseless, Squash")
    savefig(p, "relB92_squash_v2_noiseless.png")

    return p
end

function nsc_loss_squash(n=20)
    d = .9
    etas = range(0.2,1,n)
    Ks = zeros(n)

    for i in 1:n
        Ks[i] = compute_entropy_squash(d, etas[i])
    end

    p = plot(etas, Ks, dpi=300, legend=:best)
    xlabel!(p,"Transmission rate" * L"\ \eta")
    ylabel!(p,"Asympt. Key rate" * L"\ K_\infty=H(A|EC)_{|\Omega}-H(A|B)_{|\Omega}")
    title!(p,"Relativistic constraints for losses, Squash")
    ymax = max(Ks...)
    ylims!(p,0.,ymax*1.05)
    #savefig(p, "relB92_squash_losses.png")

    return [p, ds, Ks]
end

function conjecture(d,eta,q)
    return (1-d^eta)*(1-h_bin((1-d)/2)-h_bin(q))
end

function noise_behaviour_P3(n=100)
    d = .95
    qs = LinRange(0.,.05,n)
    etas = reverse(LinRange(0,1,n+1)[2:n+1])
    KR = zeros(n,n)
    for i in 1:n
        for j in 1:n
            if i>1 && j>1 && (KR[n-i+2,j] <= 1e-5 || KR[n-i+1,j-1] <= 1e-5)
                # skip if last data point was dead
                KR[n-i+1,j] = 0
            else
                KR[n-i+1,j] = asymp_key_rates_P3(d,etas[i],qs[j])
            end
        end
    end
    clamp!(KR,0.,Inf)

    KS = [qs,reverse(etas),clamp.(KR,0,Inf)]
    
    p=Plots.contourf(KS...;dpi=900,
    levels=ExpSpace(maximum(KR)*1.00001,15),guidefont=font(family="Computer Modern"),
    titlefont=font(family="Computer Modern"),xlabel="QBER "*L"q",
    ylabel="Transmittance of channel "*L"\eta",title="Protocol 3 Key Rate "*L"K(\infty)"*" for "*L"d=0.95",c=cgrad(:roma))
    Plots.savefig(p,"Protocol_3_noise.png")
    return p
end

function ExpSpace(b,n)
    xs = LinRange(log(1e-3),log(b),n-2)
    ys = exp.(xs)
    pushfirst!(ys,1e-5)
    pushfirst!(ys,-.001)
    return ys
end


function noise_behaviour_conjecture(n=100)
    d = .9
    qs = LinRange(0,.2,n)
    etas = reverse(LinRange(0,1,n))
    KR = zeros(n,n)
    for i in 1:n
        for j in 1:n
            if i>1 && j>1 && KR[n-i+2,j-1] < 1e-6
                # skip if last data point was small aready
                KR[n-i+1,j] = 0
            else
                KR[n-i+1,j] = conjecture(d,etas[i],qs[j])
            end
        end
    end
    clamp!(KR,0.,Inf)

    KS = [qs,reverse(etas),clamp.(KR,0,Inf)]
    
    p=Plots.contourf(KS...;dpi=900,levels=ExpSpace(maximum(KR),15),guidefont=font(family="Computer Modern"),titlefont=font(family="Computer Modern"),xlabel="QBER "*L"q",ylabel="Transmittance of channel "*L"\eta",title="Conjectured Key Rate "*L"K(\infty)"*" for "*L"d=0.9",c=cgrad(:roma))
    savefig(p,"Conjecture_noise.png")
    return p
end


"""
    TeXns !!! MUST !!! be of the form [L"n=10^{12}" L"n=10^9"] WITHOUT comma
"""
function do_overlap_P3(steps,ns,TeXns,num=50)
    ds_nl_P3, key_rates_nl_P3, opts_nl_P3 = finite_size_kr_P3_overlap(steps,ns,num)
    p_nl = plot_kr_overlap_P3(ds_nl_P3, key_rates_nl_P3,TeXns)
    return [p_nl,ds_nl_P3, key_rates_nl_P3, opts_nl_P3]
end

function do_losses_P3(steps,ns,TeXns,d=0.6911156094384834,num=50)
    ds_loss_P3, key_rates_loss_P3, opts_loss_P3 = finite_size_kr_P3_loss(steps,ns,d,num)
    p_loss = plot_kr_loss_P3(ds_loss_P3, key_rates_loss_P3,TeXns,d)
    return [p_loss,ds_loss_P3, key_rates_loss_P3, opts_loss_P3]
end

plot_font = "Computer Modern"
default(fontfamily=plot_font,
        linewidth=2, framestyle=:box, label=nothing, grid=false)
#scalefontsizes(3)

#num 50 recommended
#p_nl,ds_nl_P3, key_rates_nl_P3, opts_nl_P3 = do_overlap_P3(20,[10^12,10^9,10^7],[L"n=10^{12}" L"n=10^9" L"n=10^7"],50)

d = 0.9102501032166009
#d = optimal_overlap()

#p = nsc_squash(30)

#num 50 recommended
#p_loss,ds_loss_P3, key_rates_loss_P3, opts_loss_P3 = do_losses_P3(70,[10^12,10^9,10^7],[L"n=10^{12}" L"n=10^9" L"n=10^7"],d,30) # This doesn't change much... Maybe try more

p = noise_behaviour_P3(100)